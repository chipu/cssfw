const gulp = require('gulp')
const sass = require('gulp-sass')

// CSS
gulp.task('sass', function() {
    return gulp.src('src/*.scss')
        .pipe(sass()).on('error', sass.logError)
        .pipe(gulp.dest('css'));
});

// Watch files
gulp.task('watch', [], function() {
    gulp.watch('src/**/*.scss', ['sass']);
});

// Default task
gulp.task('default', ['sass', 'watch']);
